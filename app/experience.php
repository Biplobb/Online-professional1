<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class experience extends Model
{
    protected $fillable = [
        'company_name', 'position','work_from',
        'work_to', 'work_description'
    ];
}
