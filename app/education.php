<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class education extends Model
{
    protected $fillable = [
        'education_img','degree',
        'subject', 'institute', 'date_from','date_to',
        'education_description'
    ];
}
