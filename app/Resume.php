<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Resume extends Model
{
    protected $fillable = [
        'img', 'name','headline','short_description','location',
        'website', 'salary','age','mobile',
        'email', 'tag','resume_file','cover_img','facebook','google_plus',
        'dribble', 'pinterest', 'twitter','github',
        'instagram', 'youtube'
    ];
}
