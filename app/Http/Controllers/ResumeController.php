<?php

namespace App\Http\Controllers;

use App\education;
use App\Resume;
use App\skill;
use DB;
use Illuminate\Http\Request;
use function view;

class ResumeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:jobseeker');
    }

    public function index()
    {
        return view('jobseeker.resume.resume-detail');
    }

    public function create()
    {
        return view('jobseeker.resume.create-resume');
    }

    public function store(Request $request)
    {
/*return $request;*/
        {
            $img=$request->file('img');
            $img_name=$img->getClientOriginalName();
            $ext= $request->img->getClientOriginalExtension();
            $img_name=time().'.'.$ext;
            $upload_path_for_img='uploaded_files/resume-img/';
            $img->move( $upload_path_for_img,$img_name);


            $resume_file=$request->file('resume_file');
            $img_name1=$resume_file->getClientOriginalName();
            $ext1= $request->resume_file->getClientOriginalExtension();
            $img_name1=time().'.'.$ext1;
            $upload_path_for_img1='uploaded_files/resume-img/';
            $resume_file->move( $upload_path_for_img1,$img_name1);




            $cover_img=$request->file('cover_img');
            $img_name2=$cover_img->getClientOriginalName();
            $ext2= $request->cover_img->getClientOriginalExtension();
            $img_name2=time().'.'.$ext2;
            $upload_path_for_img2='uploaded_files/resume-img/';
            $cover_img->move( $upload_path_for_img2,$img_name2);
            if($request->file('experience_img'))
            {
                $img_name6[]=array();
                foreach($request->file('experience_img') as $experience_img)
                {
                    if(!empty($experience_img))
                    {
                        $destinationPath = 'uploaded_files/experience-img/';
                        $filename = $experience_img->getClientOriginalName();
                        $img_name5=time().'.'.$filename;
                        $img_name6[]= $img_name5;
                        $experience_img->move($destinationPath, $img_name5);


                    }
                }
            }


   /* dd($img_name6['2']);*/

 if($request->file('education_img'))
            {
                foreach($request->file('education_img') as $education_img)
                {
                    if(!empty($education_img))
                    {
                        $destinationPath = 'uploaded_files/resume-img/';
                        $filename = $education_img->getClientOriginalName();
                        $img_name3=time().'.'.$filename;
                        $img_name7[]= $img_name3;
                        $education_img->move($destinationPath, $img_name3);


                    }
                }
            }


/*
             $education_img=$request->file('education_img');
            $img_name3=$education_img->getClientOriginalName();
            $ext3= $request->education_img->getClientOriginalExtension();
            $img_name3=time().'.'.$ext3;
            $upload_path_for_img3='uploaded_files/resume-img/';
            $education_img->move( $upload_path_for_img3,$img_name3);*/



            if (Resume::create([
                'img'=>$img_name,
                'name'=>$request->name,
                'headline'=>$request->headline,
                'short_description'=>$request->short_description,
                'location'=>$request->location,
                'website'=>$request->website,
                'salary'=>$request->salary,
                'age'=>$request->age,
                'mobile'=>$request->mobile,
                'email'=>$request->email,
                'tag'=>$request->tag,
                'img'=>$img_name,
                'resume_file'=>$img_name1,
                'cover_img'=>$img_name2,
                'facebook'=>$request->facebook,
                'google_plus'=>$request->google_plus,
                'dribble'=>$request->dribble,
                'pinterest'=>$request->pinterest,
                'twitter'=>$request->twitter,
                'github'=>$request->github,
                'instagram'=>$request->instagram,
                'youtube'=>$request->youtube,
               /* 'education-img'=>$request->education_img,
                'degree'=>$request->degree,
                'subject'=>$request->subject,
                'institute'=>$request->institute,
                'date-from'=>$request->date_from,
                'date-to'=>$request->date_to,
                'education_description'=>$request->education_description,
                'company_name'=>$request->company_name,
                'position'=>$request->position,
                'work_from'=>$request->work_from,
                'work_to'=>$request->work_to,
               */

            ]))


                $b=sizeof($request->degree)-1;
                $c=1;
            for ($i =0; $i <$b; $i++) {

                $ids[] = DB::table('experience')->insertGetId(['experience_img' =>$img_name6[$c],'company_name' => $request->company_name[$i],'position' =>$request->position[$i],'work_from' => $request->work_from[$i],'work_to' =>$request->work_to[$i],'work_description' => $request->work_description[$i],'created_at' => now(), 'updated_at' => now()]);
                 $c++;
            }

                $b=sizeof($request->company_name)-1;
                     $c=0;
            for ($i =0; $i <$b; $i++) {

                $ids[] = DB::table('education')->insertGetId(['degree' => $request->degree[$i],'subject' => $request->subject[$i],'education_img' =>'3','institute' => $request->institute[$i],'date_from' => $request->date_from[$i],'date_to' => $request->date_to[$i],'education_description' => $request->education_description[$i],'created_at' => now(), 'updated_at' => now()]);
                      $c++;
            }




                $a=sizeof($request->skill_name)-1;
                for ($i =0; $i <$a; $i++) {

                    $ids[] = DB::table('skill')->insertGetId(['skill_name' => $request->skill_name[$i],'skill_percent' => $request->skill_percent[$i],'created_at' => now(), 'updated_at' => now()]);

                }




            {
                return redirect('resume-detail');
            }


        }
    }


    public function show(Resume $resume)
    {
        //
    }


    public function edit(Resume $resume)
    {
        //
    }


    public function update(Request $request, Resume $resume)
    {
        //
    }


    public function destroy(Resume $resume)
    {
        //
    }
}
