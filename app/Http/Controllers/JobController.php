<?php

namespace App\Http\Controllers;

use App\Job;
use Illuminate\Http\Request;
use function view;
use Illuminate\Auth;
use Illuminate\Auth\Authenticatable;
use compact;
use App\Http\Controllers\Auth\EmployerLoginController;
use App\Http\Controllers\Controller;
use App\Company;
use function compact;
use function dd;
use Illuminate\Support\Facades\DB;
use function redirect;

class JobController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:employer',['except'=>['index','indexjobs']]);
    }

    public function index()
    {
        $jobs=Job::all();
        return view('job.job-list',compact('jobs'));
    }
    public function indexjobs()
    {
        //$jobs=Job::all();
        $jobs=DB::table('jobs')->orderBy('id','asc')->take(2)->select('*')->get();
        return view('index',compact('jobs'));
    }


    public function create()
    {
        return view('job.create');
    }
    public function detail()
    {
        $jobs=Job::select('tittle','company','short_description','url','location','type',
            'salary','hours','experience','degree','img','description')->
        where('id','=',\Illuminate\Support\Facades\Auth::guard('employer')->user()->id)->get()->first();
        return view('job.job-detail',compact('jobs'));
    }

    public function store(Request $request)
    {
        //return $request;
        if ($request->hasFile('img')){
            $img=$request->file('img');
            $img_name=$img->getClientOriginalName();
            $ext= $request->img->getClientOriginalExtension();
            $img_name=time().'.'.$ext;
            $upload_path_for_img='uploaded_files/job-img/';
            $img->move( $upload_path_for_img,$img_name);
        }
        if (Job::create([
            'tittle' =>$request->tittle,
            'company' => $request->company,
            'short_description' => $request->short_description,
            'url' => $request->url,
            'location' => $request->location,
            'type' => $request->type,
            'salary' => $request->salary,
            'hours' => $request->hours,
            'experience' => $request->experience,
            'degree' => $request->degree,
            'img' =>$img_name,
            'description' => $request->description,
            'status' => 'o',
        ])){
            return redirect('job-detail');
        }
    }


    public function manage()
    {
      $jobs[]=array();
     /*   for ($i =1; $i<3; $i++) {*/

             $jobs[]=DB::table('jobs')->select('*')->
            where('status','=','0')->get();

            return view('job.manage-jobs',compact('jobs'));
        }





    public function show(Job $job)
    {
        //
    }


    public function edit(Job $job)
    {
        //
    }


    public function update(Request $request, Job $job)
    {
        //
    }


    public function destroy(Job $job)
    {
        //
    }
}
