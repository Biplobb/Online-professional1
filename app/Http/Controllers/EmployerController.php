<?php

namespace App\Http\Controllers;

use App\employer;
use Illuminate\Http\Request;
use function view;

class EmployerController extends Controller
{
    public function index()
    {
        //
    }


    public function create()
    {
        return view('employer.register');
    }


    public function store(Request $request)
    {
        if($request->password_confirmation==$request->password){
            if( employer::create([
                'name' => $request->name,
                'email' => $request->email,
                'password' => bcrypt($request->password),
            ])){
                //return "Successfull";
            return redirect('employer-login-form');
            }
        }
        return redirect('employer-register');

    }


    public function show(employer $employer)
    {
        //
    }


    public function edit(employer $employer)
    {
        //
    }


    public function update(Request $request, employer $employer)
    {
        //
    }


    public function destroy(employer $employer)
    {
        //
    }
}
