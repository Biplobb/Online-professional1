<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    protected $fillable = [
        'tittle','company','short_description','url','location','type',
        'salary','hours','experience','degree','img','description'
    ];
}
