<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompaniesTable extends Migration
{

    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->increments('id');
            $table->string('company_img');
            $table->string('company_name');
            $table->string('company_heading');
            $table->string('company_description');
            $table->string('location');
            $table->string('employer_number');
            $table->string('company_website');
            $table->string('company_foundation');
            $table->string('company_mobile');
            $table->string('company_email');
            $table->string('company_cover_img');
            $table->string('facebook');
            $table->string('google_plus');
            $table->string('dribbble');
            $table->string('pinterest');
            $table->string('twitter');
            $table->string('github');
            $table->string('instagram');
            $table->string('youtube');
            $table->string('company_details');
            $table->timestamps();
        });
    }


    public function down()
    {
        Schema::dropIfExists('companies');
    }
}
