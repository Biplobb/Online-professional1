<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobsTable extends Migration
{

    public function up()
    {
        Schema::create('jobs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('tittle');
            $table->string('company');
            $table->string('short_description');
            $table->string('url');
            $table->string('location');
            $table->string('type');
            $table->string('salary');
            $table->string('hours');
            $table->string('experience');
            $table->string('degree');
            $table->string('img');
            $table->string('description');
            $table->string('status');
            $table->timestamps();
        });
    }


    public function down()
    {
        Schema::dropIfExists('jobs');
    }
}
