<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResumesTable extends Migration
{

    public function up()
    {
        Schema::create('resumes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('img')->nullable();
            $table->string('name')->nullable();
            $table->string('headline')->nullable();
            $table->string('short_description')->nullable();
            $table->string('location')->nullable();
            $table->string('website')->nullable();
            $table->string('salary')->nullable();
            $table->string('age')->nullable();
            $table->string('mobile')->nullable();
            $table->string('email')->nullable();
            $table->string('tag')->nullable();

            $table->string('resume_file')->nullable();
            $table->string('cover_img')->nullable();
            $table->string('facebook')->nullable();
            $table->string('google_plus')->nullable();
            $table->string('dribble')->nullable();
            $table->string('pinterest')->nullable();
            $table->string('twitter')->nullable();
            $table->string('github')->nullable();
            $table->string('instagram')->nullable();
            $table->string('youtube')->nullable();
           /* $table->string('education_img')->nullable();
            $table->string('degree')->nullable();
            $table->string('subject')->nullable();
            $table->string('institute')->nullable();
            $table->string('date_from')->nullable();
            $table->string('date_to')->nullable();
            $table->string('education_description')->nullable();
            $table->string('company_name')->nullable();
            $table->string('position')->nullable();
            $table->string('work_from')->nullable();
            $table->string('work_to')->nullable();
            $table->string('work_description')->nullable();
            $table->string('skill_name')->nullable();
            $table->string('skill_percent')->nullable();*/
            $table->timestamps();
        });
    }


    public function down()
    {
        Schema::dropIfExists('resumes');
    }
}
